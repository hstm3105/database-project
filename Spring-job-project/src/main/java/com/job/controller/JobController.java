package com.job.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.job.exception.DeptNotFoundException;
import com.job.exception.JobNotFoundException;
import com.job.exception.LocationNotFoundException;
import com.job.exception.ProfileNotFoundException;
import com.job.exception.TypeNotFoundException;
import com.job.model.Job;
import com.job.model.Skills;
import com.job.service.JobService;

@RestController
@RequestMapping("/job-portal")
public class JobController {
	
	@Autowired
	JobService jobservice;

	@GetMapping("/home")
	public String showjob() {
		return "Welcome to SG Job Portal";
	}

	@GetMapping("/jobs")
	public List<Job> showAllJobs() throws JobNotFoundException {
		List<Job> jobList = new ArrayList<>();
		try {
			jobList = jobservice.showAllJobs();
		} catch (Exception e) {
			throw new JobNotFoundException("No jobs Found ");
		}
		return jobList;
	}

	@GetMapping("/job/location/{location}")
	public List<Job> showJobsByLocation(@PathVariable("location") String location) throws LocationNotFoundException {
		List<Job> jobList = new ArrayList<>();
		try {
			jobList = jobservice.searchByLocation(location);
		} catch (Exception e) {
			throw new LocationNotFoundException("No jobs Found ");
		}
		return jobList;
	}

	@GetMapping("/job/dept/{dept}")
	public List<Job> showJobsByDept(@PathVariable("dept") String dept) throws DeptNotFoundException {
		List<Job> jobList = new ArrayList<>();
		try {
			jobList = jobservice.searchByDept(dept);
		} catch (Exception e) {
			throw new DeptNotFoundException("No jobs Found ");
		}
		return jobList;
	}

	@GetMapping("/job/profile/{profile}")
	public List<Job> showJobsByProfile(@PathVariable("profile") String profile) throws ProfileNotFoundException {
		List<Job> jobList = new ArrayList<>();
		try {
			jobList = jobservice.searchByProfile(profile);
		} catch (Exception e) {
			throw new ProfileNotFoundException("No jobs Found ");
		}
		return jobList;
	}

	@GetMapping("/job/type/{type}")
	public List<Job> showJobsByType(@PathVariable("type") String type) throws TypeNotFoundException {
		List<Job> jobList = new ArrayList<>();
		try {
			jobList = jobservice.searchByType(type);
		} catch (Exception e) {
			throw new TypeNotFoundException("No jobs Found ");
		}
		return jobList;
	}

	@GetMapping("/job/skills/{skillsType}")
	public List<Job> showJobsBySkill(@PathVariable("skillsType") String[] skillsType) throws JobNotFoundException {
		
		List<Job> jobList = new ArrayList<>();
		try {
			jobList = jobservice.searchBySkillSet(skillsType);
		} catch (Exception e) {
			throw new JobNotFoundException("No jobs Found ");
		}
		return jobList;
	}
	
	@GetMapping("/job/skillsAndExperience/{skillsType}/{experience}")
	public List<Job> showJobsBySkillAndExperience(@PathVariable("skillsType") String[] skillsType, @PathVariable("experience") int experience) throws JobNotFoundException {
		
		List<Job> jobList = new ArrayList<>();
		try {
			jobList = jobservice.searchBySkillAndExperience(skillsType, experience);
		} catch (Exception e) {
			throw new JobNotFoundException("No jobs Found ");
		}
		return jobList;
	}


	@GetMapping("/job/jobId/{jobId}")
	public Job showJobsByType(@PathVariable("jobId") int jobId) throws JobNotFoundException {
		Job job = new Job();
		try {
			job = jobservice.getJobById(jobId);
		} catch (Exception e) {
			throw new JobNotFoundException("No jobs Found ");
		}
		return job;
	}

	@PostMapping("/jobs")
	public String postJob(Job job) {
		System.out.println(job);
		Set<Skills> skillset = job.getSkillSet();
		for (Skills skill : skillset) {
			skill.getJoblist().add(job);
		}
		jobservice.postJob(job);
		return "Job posted successfully";
	}

	@PostMapping("/job/editJob")
	public String editJob(Job job) {
		jobservice.editJob(job);
		return "Job edited successfully";
	}

	@PostMapping("/job/deleteJob")
	public String deleteJob(Job job) {
		jobservice.deleteJob(job);
		return "Job deleted successfully";
	}
}
