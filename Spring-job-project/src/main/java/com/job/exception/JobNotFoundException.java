package com.job.exception;

public class JobNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JobNotFoundException() {
		super();
	}

	public JobNotFoundException(String message) {
		super(message);
	}
	
}
