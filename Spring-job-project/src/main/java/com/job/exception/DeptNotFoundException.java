package com.job.exception;

public class DeptNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DeptNotFoundException() {
		super();
	}

	public DeptNotFoundException(String message) {
		super(message);
	}
	
}
