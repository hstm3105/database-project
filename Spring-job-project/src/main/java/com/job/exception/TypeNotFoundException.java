package com.job.exception;

public class TypeNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TypeNotFoundException() {
		super();
	}

	public TypeNotFoundException(String message) {
		super(message);
	}
	
}
