package com.job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
//@EnableEurekaClient
public class SpringJobProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringJobProjectApplication.class, args);
	}

}
