package com.job.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.job.model.Job;

@Repository
public interface JobDAO extends JpaRepository<Job, Integer> {

	List<Job> findByType(String type);

	List<Job> findByDept(String dept);

	List<Job> findByLocation(String location);

	List<Job> findByProfile(String profile);

	@Query("SELECT j FROM Job j  JOIN  j.skillSet s WHERE s.skillType IN  (:skills)")
	List<Job> findBySkills(@Param("skills") String[] skills);
	
	@Query("SELECT j FROM Job j  WHERE j.experience >= ?1")
	List<Job> findByExperience(Integer experience);



}
