package com.job.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.job.model.Skills;

@Repository
public interface SkillsDAO extends JpaRepository<Skills, Integer> {
	@Query("SELECT COUNT(*) FROM Skills S WHERE S.skillType=?1")
	int countSkills(String skillType);

}
