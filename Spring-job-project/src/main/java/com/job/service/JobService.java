package com.job.service;

import java.util.List;

import com.job.exception.DeptNotFoundException;
import com.job.exception.JobNotFoundException;
import com.job.exception.LocationNotFoundException;
import com.job.exception.ProfileNotFoundException;
import com.job.exception.TypeNotFoundException;
import com.job.model.Job;

public interface JobService {
	void postJob(Job job);

	List<Job> searchByLocation(String location) throws LocationNotFoundException;

	List<Job> searchByDept(String dept) throws DeptNotFoundException;

	List<Job> searchByType(String type) throws TypeNotFoundException;

	void deleteJob(Job job);

	void editJob(Job job);

	Job getJobById(int jobId) throws JobNotFoundException;

	List<Job> searchByProfile(String profile) throws ProfileNotFoundException;


	List<Job> showAllJobs();

	List<Job> searchBySkillSet(String[] skills) throws JobNotFoundException;

	List<Job> searchBySkillAndExperience(String[] skillsType, int experience);

	List<Job> searchByExperience(int experience) throws JobNotFoundException;
}
