package com.job.service;

import com.job.model.Skills;

public interface SkillService {
	void postSkills(Skills skill);
	boolean checkSkills(Skills skill);
}
