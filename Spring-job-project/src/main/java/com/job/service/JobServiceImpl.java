package com.job.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.job.dao.JobDAO;
import com.job.exception.DeptNotFoundException;
import com.job.exception.JobNotFoundException;
import com.job.exception.LocationNotFoundException;
import com.job.exception.ProfileNotFoundException;
import com.job.exception.TypeNotFoundException;
import com.job.model.Job;


@Service
public class JobServiceImpl implements JobService {
	
	@Autowired
	JobDAO jobdao;

	@Override
	public void postJob(Job job) {
		jobdao.save(job);
	}

	@Override
	public List<Job> searchByLocation(String location) throws LocationNotFoundException {
		return jobdao.findByLocation(location);
	}

	@Override
	public List<Job> searchByDept(String dept) throws DeptNotFoundException {
		return jobdao.findByDept(dept);
	}

	@Override
	public List<Job> searchByType(String type) throws TypeNotFoundException {
		return jobdao.findByType(type);
	}

	@Override
	public void deleteJob(Job job) {
		jobdao.delete(job);
	}

	@Override
	public void editJob(Job job) {
		jobdao.save(job);
	}

	@Override
	public Job getJobById(int jobId) throws JobNotFoundException {
		return jobdao.getOne(jobId);
	}

	@Override
	public List<Job> searchByProfile(String profile) throws ProfileNotFoundException {
		return jobdao.findByProfile(profile);
	}

	@Override
	public List<Job> searchBySkillSet(String[] skills) throws JobNotFoundException {
		for (String newskill : skills) {
			System.out.println(newskill);
		}
		return jobdao.findBySkills(skills);
	}

	@Override
	public List<Job> searchByExperience(int experience) throws JobNotFoundException {
		return jobdao.findByExperience(experience);
	}

	@Override
	public List<Job> showAllJobs() {
		return jobdao.findAll();
	}

	@Override
	public List<Job> searchBySkillAndExperience(String[] skills, int experience) {
		List<Job> list1=jobdao.findBySkills(skills);
		List<Job> list2=jobdao.findByExperience(experience);
		list1.retainAll(list2);
		return list1;
//		return jobdao.findBySkillAndExperience(skills, experience);
	}

}
