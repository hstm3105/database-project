package com.job.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.job.dao.SkillsDAO;
import com.job.model.Skills;

@Service
public class SkillServiceImpl implements SkillService {

	@Autowired
	SkillsDAO skillsdao;

	@Override
	public void postSkills(Skills skill) {

	}

	@Override
	public boolean checkSkills(Skills skill) {

		return skillsdao.countSkills(skill.getSkillType()) > 0;
	}
}
